# ESP8266 with µPython setup

Procedure to flash a ESP8266 as a µPython programmable µcontroller. This was made using Windows 10.

## Requirements 

- Installed Python (https://www.python.org/downloads/)
(check by typing on the command prompt (cmd) `python --version`)

## Install µPython
1. Install esptools by typing the command `pip3 install esptool`
2. Connect the board to the computer and determine the port (ex.: COM9)
3. Flash the µcontroller using `esptool.py --port COM9 erase_flash` (remember to change the port)
4. Download the script at http://micropython.org/download/esp8266/
5. Deploy firmware `esptool.py --port COM9 --baud 115200 write_flash --flash_size=detect 0 esp8266-20191220-v1.12.bin`
    - Make sure you have the right port 
    - The baudrange might change 
    - Make sure the file name is the same one you downloaded 
    - deploy at `cd downloads`

## Transfer programms to the µcontroller
1. Download Thonny (https://thonny.org/)
2. Set up the IDE @Tools> Options 
    - Interpreter: MicroPython (generic)
    - Select the port 

### Run example (Blink)
1. download main.py 
2. open main.py with Thonny 
3. Save the file> on the µcontroller
4. Run 

## Sources 

- video tutorial:  https://www.youtube.com/watch?v=dY0QyILX8NA 
- Intro: https://docs.micropython.org/en/latest/esp8266/tutorial/intro.html

For ESP32 look at: https://how2electronics.com/esp32-micropython-upycraft-getting-started/